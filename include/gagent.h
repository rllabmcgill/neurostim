/****************************
 *
 * Keith Bush (2009)
 *
 * Reasoning and Learning Lab
 * McGill University
 *
 ****************************/

#ifndef GAGENT_H
#define GAGENT_H

#include "model.h"

namespace ublas = boost::numeric::ublas;
using namespace std;

class Agent{

 public:
  
  Agent(int ntile, int naction, int dfeature, double alph, double gam);
  void setrange(int dim, double rmin, double rmax);
  int getindex(const ublas::vector<double> & feature);
  double getreward(int);
  double getqvalue(const ublas::vector<double> &, int);
  void setqvalue(const ublas::vector<double> &, int, float);  
  double getabsqsum();
  int greedyactionid(const ublas::vector<double> &);
  void sarsa(const ublas::vector<double> & data, int action);

 private:

  int Ntile;    //Number of tiles
  int Dfeature; //dimension of features
  int Naction;  //Number of actions in representation
  int Wsize;    //Total number of indices in the weights
  
  double alpha; //Learning rate
  double gamma; //Discount factor

  ublas::matrix<double,ublas::column_major> weights;
  ublas::matrix<double,ublas::column_major> bounds;
  
};

#endif
