CC = g++
CFLAGS = -O2 -DNDEBUG -march=native
INCDIR = -I. -I./include/ -I$(HOME)/boost_1_34_0/ 
LIBDIR = -L /usr/lib/atlas/
LIB = -llapack -lblas -lm
OBJS = model.o kdtree2.o matvecops.o gagent.o 

main: fixed_suppression rl_lut_suppression 

##Experiments
fixed_suppression: ./src/experiment/fixed_suppression.cpp $(OBJS)
	$(CC) $(CFLAGS) $(INCDIR) -o $@ $< $(OBJS) $(LIB) $(LIBDIR)

rl_lut_suppression: ./src/experiment/rl_lut_suppression.cpp $(OBJS)
	$(CC) $(CFLAGS) $(INCDIR) -o $@ $< $(OBJS) $(LIB) $(LIBDIR)

##RL Objects
gagent.o: ./src/rl/gagent.cpp
	$(CC) $(CFLAGS) $(INCDIR) -c $<

##System Objects
model.o: ./src/system/model.cpp 
	$(CC) $(CFLAGS) $(INCDIR) -c $<

##Utility Functions
matvecops.o: ./src/utility/matvecops.cpp
	$(CC) $(CFLAGS) $(INCDIR) -c $<

kdtree2.o: ./src/utility/kdtree2.cpp
	$(CC) $(CFLAGS) $(INCDIR) -c $<

clean:
	rm -f include/*~ src/*/*~ *~ *.o fixed_suppression rl_lut_suppression





