/****************************
 *
 * Keith Bush (2009)
 *
 * Reasoning and Learning Lab
 * McGill University
 *
 ****************************/

#include<cassert>
#include <boost/numeric/bindings/traits/ublas_matrix.hpp>
#include <boost/numeric/bindings/traits/ublas_vector.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>
#include <boost/numeric/ublas/vector_proxy.hpp>
#include <boost/numeric/ublas/io.hpp>

#include <iostream>
#include <string>
#include <cstdlib> 
#include <cmath>

#include "model.h"
#include "gagent.h"
#include "matvecops.h"

namespace ublas = boost::numeric::ublas;
using namespace std;

Agent::Agent(int ntile, int naction, int dfeature, double alph, double gam){
  
  Ntile=ntile;        //Number of tiles
  Dfeature=dfeature;  //Dimension of features
  Naction=naction;    //Number of actions in representation

  alpha=alph;         //Learning rate
  gamma=gam;          //Discount factor
  
  //JUST FOR LOOKUP TABLE
  Wsize=Ntile;
  for(int i=0;i<(Dfeature-1);i++){
    Wsize*=Ntile;
  }
  weights = zero_map(Naction,Wsize);
  bounds = zero_map(Dfeature,3);

}

void Agent::setrange(int dim, double rmin, double rmax){

  bounds(dim,0)=rmin;
  bounds(dim,1)=rmax;
  bounds(dim,2)=(rmax-rmin)/Ntile;
  
}


int Agent::getindex(const ublas::vector<double> & feature){

  double feat;
  double step;
  double imin;

  ublas::vector<int> indices(feature.size());

  for(int i=0;i<feature.size();i++){

    feat = feature(i);
    step = bounds(i,2);
    imin = bounds(i,0);
    
    int index = 0;
    double place = imin+step;
    while(place<feat){
      index++;
      place+=step;
    }

    
    if(index>=Ntile){
      index=(Ntile-1);
    }

  
    indices(i)=index;

  }

  //Assemble dimension indices into single index
  ublas::vector<int> powers(Dfeature);
  for(int i=(Dfeature-1);i>=0;i--){
    powers((Dfeature-1)-i)=(int)pow((double)Ntile,(double)i);
  }

  int tindex=0;
  for(int i=0;i<feature.size();i++){
    tindex = tindex + indices(i)*powers(i);
  }
  
  return(tindex);

}

double Agent::getqvalue(const ublas::vector<double> & feature, int actionid){
  return(weights(actionid,getindex(feature)));
}

double Agent::getabsqsum(){
  double qsum=0.0;
  
  //Sum over all Q-values
  for(int i=0;i<Naction;i++){
    for(int j=0;j<Wsize;j++){
      qsum+=abs(weights(i,j));
    }
  }

  return(qsum);

}

void Agent::setqvalue(const ublas::vector<double> & feature, int actionid, float value){
  weights(actionid,getindex(feature))= value;
}


int Agent::greedyactionid(const ublas::vector<double> & feature){


  int curractionid = -1;
  int bestactionid = -1;

  double currq = -999999.0;
  double bestq = -999999.0;

  ublas::vector<double> tiebreak(Naction);
  int tie=0;

  //Find max action
  for(int nn=0;nn<Naction;nn++){

    curractionid = nn;
    currq = weights(curractionid,getindex(feature));

    if(currq>bestq){
      bestq=currq;
      bestactionid=curractionid;
    }else{
      if(currq==bestq){
	tiebreak(bestactionid)=1.0;
	tiebreak(curractionid)=1.0;
	tie=1;
      }
    }
  }
  
  //Break ties
  if(tie){

    int Ntie=0;
    int tieactionid=-1;
    double maxrand=-1.0;
    double prand = 1.0;

    for(int nn=0;nn<Naction;nn++){
      if(tiebreak(nn)>0){
	prand = (double)rand()/RAND_MAX;
	if(prand>maxrand){
	  maxrand=prand;
	  tieactionid=nn;
	}
      }
    }

    bestactionid=tieactionid;

  }

  return(bestactionid);

}


double Agent::getreward(int isgoal){

  if(isgoal){
    return(0.0);
  }else{
    return(-1.0);
  }

}

void Agent::sarsa(const ublas::vector<double> & data, int action){

  ublas::vector<double> prevstate = subrange(data,0,Dfeature);
  ublas::vector<double> currstate = subrange(data,Dfeature,2*Dfeature);

  int  curractionid = action;
  int  prevactionid = (int)data(2*Dfeature);
  double currreward = data(2*Dfeature+1);

  double curr_Qvalue = weights(curractionid,getindex(currstate));
  double prev_Qvalue = weights(prevactionid,getindex(prevstate));
  double tderror = currreward + gamma*curr_Qvalue - prev_Qvalue;

  weights(prevactionid,getindex(prevstate)) += alpha*tderror;

}





