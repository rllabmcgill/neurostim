/****************************
 *
 * Keith Bush (2009)
 *
 * Reasoning and Learning Lab
 * McGill University
 *
 ****************************/

#include <cassert>
#include <boost/numeric/bindings/traits/ublas_matrix.hpp>
#include <boost/numeric/bindings/traits/ublas_vector.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>
#include <boost/numeric/ublas/vector_proxy.hpp>
#include <boost/numeric/ublas/io.hpp>

#include <iostream>
#include <cstdlib> 
#include <ctime> 
#include <cmath>

#include "gagent.h"
#include "model.h"
#include "matvecops.h"

namespace ublas = boost::numeric::ublas;
using namespace std;

int main(int argc, char** argv){
  
  //-------------------------------------------
  //------------ Read In Cmd Line -------------
  //-------------------------------------------
  if(argc!=12){
    cout << " " << endl;
    cout << "usage: rl_lut_suppression <init> <noise> <R> <alpha> <gamma> <apen> <spen> <E> <Tmin> <Nobs> <Nmax>" << endl;
    cout << "  " << endl;
    cout << "       Simulation parameters:" << endl;
    cout << "           <init> = model configuration file" << endl;              //params/params.dat
    cout << "           <noise> = numerical integration noise" << endl;          //0.0001
    cout << "       RL parameters:" << endl;				         
    cout << "           <R> = tile resolution of lookup table" << endl;          //21
    cout << "           <alpha> = learning rate" << endl;                        //0.01
    cout << "           <gamma> = discount factor" << endl;                      //0.9
    cout << "           <apen> = action penalty" << endl;                        //-1.0
    cout << "           <spen> = seizure penalty" << endl;                       //-40.0
    cout << "       Embedding parameters:" << endl;			         
    cout << "           <E> = embed dimension" << endl;                          //2
    cout << "           <Tmin> = embed window" << endl;                          //6 (1.2sec)
    cout << "       Experiment parameters:" << endl;                      
    cout << "           <Nobs> = num of observations per stat. update" << endl;  //100000
    cout << "           <Nmax> = max num of observations" << endl;               //10000000
    cout << " " << endl;
    cout << "output: [fraction seizure, fraction stimulation]" << endl;
    cout << " " << endl;
    exit(0);
  }

  //Seed random number generator
  srand((unsigned)time(0)); 

  //Experiment params
  char * init_file = argv[1];
  double noise = atof(argv[2]);

  //RL params
  int Ntile = atoi(argv[3]);
  double alpha = atof(argv[4]);
  double gamma = atof(argv[5]);
  double apen = atof(argv[6]);
  double spen = atof(argv[7]);

  //Embedding params
  int E = atoi(argv[8]);
  int Tmin = atoi(argv[9]);
  int tau = (int)Tmin/(E-1);
  int Nbuff = (E-1)*tau+1;

  //Experiment params
  int Nobs = atoi(argv[10]);
  int Nmax = atoi(argv[11]);

  //Storage
  ublas::vector<int> labels(Nobs);
  ublas::vector<int> actions(Nobs);
  
  //Construct System
  Model* model = new Model(init_file);

  //Construct and Initialize Agent
  Agent* agent = new Agent(Ntile,2,E,alpha,gamma);
  for(int ii=0;ii<E;ii++){
    agent->setrange(ii,model->get_min_by_dimension(0),model->get_max_by_dimension(0));
  }
  
  //Learning Intialization
  double reward=0;
  int prevaction = 0;
  int action=0;

  model->reset();
  ublas::vector<double> currstate = model->get_state();
  ublas::vector<double> prevstate = currstate;

  ublas::vector<double> buffer = ublas::zero_vector<float>(Nbuff);
  buffer(0) = currstate(0);
  
  ublas::vector<double> currembed = ublas::zero_vector<float>(E);
  ublas::vector<double> prevembed = ublas::zero_vector<float>(E);
  currembed(0) = buffer(0);

  //Loop until goal
  for(int cnt=0;cnt<Nmax;cnt++){

    //Select Action
    prevaction = action ;
    action = agent->greedyactionid(currembed);

    //Iterate System
    model->step(action,noise); 

    //Iterate Storage
    prevstate=currstate;
    currstate=model->get_state();

    //Compute reward
    reward = 0.0; 
    if(action>0){
	reward = apen;
    }
    if(model->get_label(model->get_neighbor())==model->get_seiz_label()){
	reward += spen;
    }

    //Store data
    if(model->get_label(model->get_neighbor())==model->get_seiz_label()){
	labels(cnt%Nobs)= 1;
    }else{
	labels(cnt%Nobs)=0;
    }

    actions(cnt%Nobs) = action;
    
    //Update BUFFER
    for(int ii=(Nbuff-2); ii>=0; ii--){
	buffer(ii+1)=buffer(ii);
    }
    buffer(0) = currstate(0); 

    //Copy old embed to new embed
    prevembed=currembed;
    
    int ecnt =0;
    for(int ii=0;ii<Nbuff;ii++){
	if(ii%tau==0){
	  //Copy from buffer to embed
	  currembed(ecnt)=buffer(ii);
	  ecnt++;
	}
    }

    //Update Agent
    ublas::vector<double> tuple = c(c(c(prevembed,currembed),action),reward);
    agent->sarsa(tuple,(double)agent->greedyactionid(currembed));

    //---- BEGIN STATISTICS --
    if((cnt>1)&(cnt%Nobs==0)){
	int lsum = 0;
	int asum = 0;
	for(int i=0;i<Nobs;i++){
	  lsum+=labels(i);
	  asum+=actions(i);
	}
	cout << (double)lsum/Nobs << " " << (double)asum/Nobs << " " << endl;
    }
    //---- END STATISTICS ----
    
  }
  
  return 0;
  
}
