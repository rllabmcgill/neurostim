/****************************
 *
 * Keith Bush (2009)
 *
 * Reasoning and Learning Lab
 * McGill University
 *
 ****************************/

#include <cassert>
#include <boost/numeric/bindings/blas/blas.hpp>
#include <boost/numeric/bindings/traits/ublas_matrix.hpp>
#include <boost/numeric/bindings/traits/ublas_vector.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>
#include <boost/numeric/ublas/vector_proxy.hpp>
#include <boost/numeric/ublas/io.hpp>

#include <iostream>
#include <cstdlib> 
#include <ctime> 
#include <cmath>

#include "model.h"
#include "matvecops.h"

namespace ublas = boost::numeric::ublas;
using namespace std;

int main(int argc, char** argv){
  
  //-------------------------------------------
  //------------ Read In Cmd Line -------------
  //-------------------------------------------
  if(argc!=7){
    cout << " " << endl;
    cout << "usage: fixed_suppression <init> <noise> <freq> <output> <Nobs> <Nmax>" << endl;
    cout << "  " << endl;
    cout << "       Model parameters:" << endl;
    cout << "           <init> = model configuration file" << endl;
    cout << "           <noise> = numerical integration noise" << endl;
    cout << "       Stim parameters:" << endl;
    cout << "           <freq> = stimulation frequency (Hz)" << endl;
    cout << "       Experiment parameters:" << endl;
    cout << "           <output> 0 = [no output]" << endl;
    cout << "                    1 = [model state index, action]" << endl;
    cout << "                    2 = [state, action]" << endl;
    cout << "                    3 = [time, fraction seizure, fraction stimulation]" << endl;
    cout << "           <Nobs> = num. of steps to calc. performance" << endl;
    cout << "           <Nmax> = total num of simulation steps" << endl;
    cout << " " <<endl;
    exit(0);
  }

  //Seed random number generator
  srand((unsigned)time(0)); 

  //Experiment params
  char * init_file = argv[1];
  double noise = atof(argv[2]);
  double freq = atof(argv[3]);
  int mode = atoi(argv[4]);
  int Nobs = atoi(argv[5]);
  int Nmax = atoi(argv[6]);

  //Initialize System
  Model* model = new Model(init_file);

  //Intialize Stimulation freq
  int Nfreq = ((int)1.0/freq)*model->get_Ndt(); 

  //Initialize Storage
  ublas::vector<int> labels(Nobs);
  ublas::vector<int> stims(Nobs);

  
  //Initialize Simulation
  int stim=0;
  model->reset();

  //Simulation Loop
  for(int cnt=0;cnt<Nmax;cnt++){

    //STEP 1: Select Action
    if(cnt%Nfreq==0){
      stim = 1;
    }else{
      stim = 0;
    }

    //STEP 2: Simulation output
    switch(mode){
    case 0:
      break;
    case 1:
      cout << model->get_neighbor() << " " << stim << endl;
      break;
    case 2:
      cout << model->get_state() << " " << stim << endl;
      break;
    case 3:
      {
	//Store data
	if(model->get_label(model->get_neighbor())==model->get_seiz_label()){
	  labels(cnt%Nobs)= 1;
	}else{
	  labels(cnt%Nobs)=0;
	}
	stims(cnt%Nobs) = (int)stim;

	//Output statistics
	if((cnt>1) & (cnt%Nobs==0)){
	  int lsum = 0;
	  int ssum = 0;
	  for(int i=0;i<Nobs;i++){
	    lsum+=labels(i);
	    ssum+=stims(i);
	  }
	  cout << cnt << " " << (double)lsum/Nobs << " " << (double)ssum/Nobs << endl;
	}
      }
      break;
    }

    //STEP 3: Iterate System
    model->step(stim,noise); 

  }
  
  return 0;
  
}
